package course_schema

type Session struct {
    Fall bool                 `json:"fall"`
    Term int                  `json:"term"`
    Sections struct {       
        SectionCode string      `json:"section_code"`
        Instructors []string    `json:"section_instructors"`
        Status string           `json:"section_status"`
        Type string             `json:"section_type"`
        Time []int              `json:"section_schedule"`
    }
}

type Course struct {
    Code string         `json:"code"`
    Name string         `json:"name"`
    Description string  `json:"desc"`
    Session []Session   `json:"sess"`
    Times [][5]int	    `json:"times"`
}
